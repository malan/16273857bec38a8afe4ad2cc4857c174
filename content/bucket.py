import math

class Bucket:
    """Water bucket.
    
    Parameters
    ----------
    radius
        Radius in m.
    height
        Height in m.
    filling
        Filling fraction of the bucket (0 empty, 1 full). 
        Default filling fraction is 0.5.
    """
    def __init__(self, radius, height):
        self.radius = radius
        self.height = height
        self.filling = 0.5   # filling between 0 and 1, default is 0.5

    def fill(self, amount):
        """Add amount of water to bucket.
        
        Excess water is lost if the bucket is too small/full.

        Parameters
        ----------
        amount
            Amount of water in m^3.

        """
        bucket_volume = math.pi * self.radius**2 * self.height
        if self.filling * bucket_volume + amount >= bucket_volume:
            self.filling = 1
        else:
            self.filling = (self.filling + amount) / bucket_volume